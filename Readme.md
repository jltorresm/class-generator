Class Generator
=======

Creates PHP classes based on a connection to MySQL. Maps the structure of the 
tables of a defined schema and generates model/entity classes.

Usage: 
Go to the `class-generator` folder:

- Copy `default-config` folder to `config`.
- Open `application.php` and configure the options for your local environment.
- Execute library.

In a terminal window:

	php Main.php [--action="value"] [--help]
		--action : Executes an action. If ommited nothing executes, and this message shows.
		Options:
			check - Checks the schema and tries to map the entities.
			save  - Tries to persist the entities to files.
		--help   : Show this help message.

You can access the generator log in `class-generator/log/app.log`
