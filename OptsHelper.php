<?php

class OptsHelper
{
	private static $allowedActions = ['check', 'save'];

	public static function verify($opts)
	{
		if (!isset($opts['action']))
		{
			self::showhelp();
		}
		else if (!in_array($opts['action'], self::$allowedActions))
		{
			echo "Invalid action!\n\n";
			self::showhelp();
		}
		else
		{
			return $opts['action'];
		}
	}

	private static function showHelp()
	{
		$help = "Class Generator v1.0\n\n" .
				"usage: php Main.php [--action='value'] [--help]\n\n" .
				"  --action : Executes an action. If ommited nothing executes, and this message shows.\n" .
				"    Options:\n" .
				"      check - Checks the schema and tries to map the entities.\n" .
				"      save  - Tries to persist the entities to files.\n\n" .
				"  --help   : Show this help message.\n";

		die($help);
	}
}
