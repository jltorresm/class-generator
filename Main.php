<?php
require 'vendor/autoload.php';
require 'OptsHelper.php';

use Monolog\Logger,
	Monolog\Handler\StreamHandler,
	Generator\Connection\ConnectionFactory,
	Generator\GClass\ClassFactory,
	Generator\Dao\GClass\PhpClassDao;

class Main
{
	private static $log;

	private static function init()
	{
		self::$log = new Logger(get_called_class());
		try
		{
			$path = __DIR__ . '/log';

			if (!file_exists($path)) {
				self::$log->addWarning('No log folder found. Attempting to create...');
				mkdir('./log');
			}

			self::$log->pushHandler(new StreamHandler(__DIR__ . '/log/app.log', Logger::DEBUG));
		}
		catch (\Exception $e)
		{
			self::$log->addError($e->getMessage());
			die($e->getMessage());
		}
	}

	public static function run($opts)
	{
		self::init();

		$action = OptsHelper::verify($opts);

		echo "Starting 'Class Generator'", PHP_EOL;
		self::$log->addInfo("Starting 'Class Generator'");

		$connection = ConnectionFactory::create();

		$tables = $connection->getTables();

		ClassFactory::setConnection($connection);

		foreach ($tables as $table)
		{
			$classes[] = ClassFactory::create(['name' => $table['TABLE_NAME']]);
		}

		if ($action == 'save')
		{
			$phpClassDao = new PhpClassDao();

			foreach ($classes as  $class)
			{
				$phpClassDao->persist($class);
			}
		}
		
		echo "'Class Generator' Finished Correctly", PHP_EOL;
		self::$log->addInfo("'Class Generator' Finished Correctly");
	}
}

/**
 * Verify if running form command line and auto run the main class
 */
if (php_sapi_name() == 'cli')
{
	$opts = getopt('',['help', 'action:']);
	Main::run($opts);
}
