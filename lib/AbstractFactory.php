<?php namespace Generator;

/**
* Interface for factories.
*
* @author joselo
*/
interface AbstractFactory
{
	/**
	* Returns an instance of the desired class.
	* @param string Parameter needed for creation.
	* @return Object Instance of the desired class.
	*/
	public static function create($param = null);
}
