<?php namespace Generator\Connection;

use Monolog\Logger,
	Monolog\Handler\StreamHandler;

/**
* Connection has all the main attributes to connect to a database and the methods
* to extract the information schema.
*
* @author joselo
*/
class MysqlConnection implements AbstractConnection
{
	/**
	* Name for the connection.
	* @var String
	*/
	private $connectionName;

	/**
	* Name or IP Address of the server.
	* @var String
	*/
	private $hostname;

	/**
	* TCP/IP port.
	* @var String
	*/
	private $port;

	/**
	* Name for the user to connect with.
	* @var String
	*/
	private $username;

	/**
	* The user's password.
	* @var String
	*/
	private $password;

	/**
	* The schema to use as default Schema.
	* @var String
	*/
	private $defaultSchema;

	/**
	* Connection to the db.
	* @var mysqli
	*/
	private $db;

	private $log;

	/**
	* Constructor for a new connection.
	*
	* @param String $connectionName
	* @param String $hostname
	* @param String $port
	* @param String $username
	* @param String $password
	* @param String $defaultSchema
	*/
	function __construct($hostname = '127.0.0.1', $port = '3306', $username = 'root', $password = '', $defaultSchema = 'information_schema', $connectionName = null)
	{
		$this->connectionName = $connectionName;
		$this->hostname = $hostname;
		$this->port = $port;
		$this->username = $username;
		$this->password = $password;
		$this->defaultSchema = $defaultSchema;

		$this->log = new Logger(get_class($this));
		$this->log->pushHandler(new StreamHandler(__DIR__ . '/../../log/app.log', Logger::DEBUG));
	}

	public function connect()
	{
		try
		{
			$this->db = new \mysqli($this->hostname, $this->username, $this->password, $this->defaultSchema, $this->port);

			if ($this->db->connect_errno)
			{
				throw new \Exception("Error connecting to MySQL: (" . $this->db->connect_errno . ") " . $this->db->connect_error);
			}

			return true;
		}
		catch(\Exception $e)
		{
			$this->log->addError($e->getMessage());
			error_log($e->getMessage());
			return false;
		}
	}

	public function close()
	{
		$this->db->close();
	}

	public function getTables()
	{
		$return = array();

		$sql = "SELECT *
			FROM `information_schema`.`TABLES`
			WHERE `TABLE_SCHEMA` = '$this->defaultSchema'";

		$result = $this->db->query($sql) or die($this->db->error . __LINE__);

		if ($result->num_rows > 0)
		{
			while ($row = $result->fetch_assoc())
			{
				$return[] = $row;
			}
		}
		else
		{
			return null;
		}

		return $return;
	}

	public function getColumns($tableName)
	{
		$return = array();

		$sql = "SELECT *
			FROM `information_schema`.`COLUMNS`
			WHERE `TABLE_NAME` = '$tableName'
			AND `TABLE_SCHEMA` = '$this->defaultSchema'";

		$result = $this->db->query($sql) or die($this->db->error . __LINE__);

		if ($result->num_rows > 0)
		{
			while ($row = $result->fetch_assoc())
			{
				$return[] = $row;
			}
		}
		else
		{
			return null;
		}

		return $return;
	}

	public function getSchemas()
	{
		$return = array();

		$sql = "SELECT distinct(`TABLE_SCHEMA`)
			FROM `information_schema`.`tables`
			WHERE `TABLE_TYPE` = 'BASE TABLE'";

		$result = $this->db->query($sql) or die($this->db->error . __LINE__);

		if ($result->num_rows > 0)
		{
			while ($row = $result->fetch_assoc())
			{
				$return[] = $row;
			}
		}
		else
		{
			return null;
		}

		return $return;
	}

	public function getConnectionName()
	{
		return $this->connectionName;
	}

	public function getHostname()
	{
		return $this->hostname;
	}

	public function getPort()
	{
		return $this->port;
	}

	public function getUsername()
	{
		return $this->username;
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function getDefaultSchema()
	{
		return $this->defaultSchema;
	}

	public function setConnectionName(String $connectionName)
	{
		$this->connectionName = $connectionName;
	}

	public function setHostname(String $hostname)
	{
		$this->hostname = $hostname;
	}

	public function setPort(String $port)
	{
		$this->port = $port;
	}

	public function setUsername(String $username)
	{
		$this->username = $username;
	}

	public function setPassword(String $password)
	{
		$this->password = $password;
	}

	public function setDefaultSchema(String $defaultSchema)
	{
		$this->defaultSchema = $defaultSchema;
	}
}
