<?php namespace Generator\Connection;

/**
* Interface for connections.
*
* @author joselo
*/
interface AbstractConnection
{
	/**
	* Method that attempts to connect to the DB.
	* @return boolean TRUE if success, FALSE otherwise.
	*/
	public function connect();

	/**
	* Closes the connection if open.
	*/
	public function close();

	/**
	* Returns an <b>array</b> of the existing tables of the current schema.
	*
	* @return mixed Array of existing Tables or null if fails.
	*/
	public function getTables();

	/**
	* Returns the columns of the specified table.
	*
	* @param String $tableName Name of the table.
	* @return mixed Array of columns of the table or null if fails.
	*/
	public function getColumns($tableName);

	/**
	* Return all the existing schemas in the connected DB.
	* @return mixed Array of schemas of the connection or null if fails.
	*/
	public function getSchemas();
}
