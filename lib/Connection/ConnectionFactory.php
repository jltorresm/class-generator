<?php namespace Generator\Connection;

use Monolog\Logger,
	Monolog\Handler\StreamHandler,
	Generator\AbstractFactory;

class ConnectionFactory implements AbstractFactory
{
	private static $log;

	private static function init()
	{
		self::$log = new Logger(get_called_class());
		self::$log->pushHandler(new StreamHandler(__DIR__ . '/../../log/app.log', Logger::DEBUG));
	}

	public static function create($db = null)
	{
		self::init();

		//Read config file for connection details
		try
		{
			$path = __DIR__ . '/../../config/application.php';

			if (!file_exists($path)) {
				throw new \Exception("No config file found! Please verify that you copied /default-config to /config\n");    
			}

			$config = require($path);
		}
		catch (\Exception $e)
		{
			self::$log->addError($e->getMessage());
			die($e->getMessage());
		}

		//Create connection object
		$mysqlConnection = new MysqlConnection(
			$config['db']['hostname'],
			$config['db']['port'],
			$config['db']['username'],
			$config['db']['password'],
			$config['db']['schema'],
			$config['db']['name']
		);

		self::$log->addInfo('Attempting to connect to database');
		echo 'Attempting to connect to database', PHP_EOL;

		if ($mysqlConnection->connect())
		{
			self::$log->addInfo('Now connected to database');
			echo 'Now connected to database', PHP_EOL;
			return $mysqlConnection;
		}

		self::$log->addError('Error connecting to database');
		die("Error connecting to database\n");
		return null;
	}
}
