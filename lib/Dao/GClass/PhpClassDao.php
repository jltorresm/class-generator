<?php namespace Generator\Dao\GClass;

use Generator\Dao\AbstractFileDao,
	Generator\GClass\PhpClass;

class PhpClassDao extends AbstractFileDao
{
	public function persist($entity)
	{
		try
		{
			$validType = get_class(new PhpClass);

			if (!is_object($entity) || get_class($entity) != $validType)
			{
				throw new \Exception("Not a valid PhpClass object to persist: required: '$validType' given: '" . gettype($entity) . "'");
			}

			$this->filename = $entity->getName() . '.php';

			parent::persist((string)$entity);
		}
		catch (\Exception $e)
		{
			echo $e->getMessage(), PHP_EOL;
			$this->log->addError($e->getMessage());
		}
	}
}
