<?php namespace Generator\Dao;

use Monolog\Logger,
	Monolog\Handler\StreamHandler;

abstract class AbstractFileDao implements GenericDao
{
	protected $config;
	protected $log;
	protected $filename;

	private $path;

	public function __construct()
	{
		$this->log = new Logger(get_class($this));
		$this->log->pushHandler(new StreamHandler(__DIR__ . '/../../log/app.log', Logger::DEBUG));

		try
		{
			$path = __DIR__ . '/../../config/application.php';

			if (!file_exists($path)) {
				throw new \Exception("No config file found! Please verify that you copied /default-config to /config\n");
			}

			$config = require($path);

			if (isset($config['fileDao']) && isset($config['fileDao']['prefix']) && $config['fileDao']['prefix'] != '')
			{
				$this->path = $config['fileDao']['prefix'];
			}
			else
			{
				$this->path = __DIR__ . '/../../../generatedClasses/';
			}

			if (!file_exists($this->path))
			{
				if (!mkdir($this->path, 0777, true))
				{
					throw new \Exception('Could not create directory: ' . $this->path . "\n");
				}

				$this->log->addInfo('Created dir: ' . $this->path);
				echo 'Created dir: ' . $this->path, PHP_EOL;
			}
			else
			{
				$this->log->addInfo('Dir: ' . $this->path . ' already exists');
				echo 'Dir: ' . $this->path . ' already exists', PHP_EOL;
			}
		}
		catch (\Exception $e)
		{
			$this->log->addError($e->getMessage());
			die($e->getMessage());
		}
	}

	public function findAll($entity) { /* Not going to be implemented */ }

	public function findById($id) { /* Not going to be implemented */ }

	public function persist($entity)
	{
		$fileSystem = fopen($this->path . $this->filename, 'w');
		fwrite($fileSystem, $entity);
		fclose($fileSystem);
	}

	public function merge($entity) { /* Not going to be implemented */ }

	public function remove($entity) { /* Not going to be implemented */ }
}
