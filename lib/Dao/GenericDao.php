<?php namespace Generator\Dao;

/**
* Generic Data Access Object
*
* @author joselo
*/
interface GenericDao
{

	/**
	* Finds <b>every</b> object of a given Entity.
	* @param String $entity DesiredEntity.
	* @return array<Object> Found Entities.
	*/
	public function findAll($entity);

	/**
	* Finds an Entity by <b>ID</b>.
	* @param Mixed $id Entity identifier.
	* @return Object Found entity.
	*/
	public function findById($id);

	/**
	* <b>Saves</b> a new object to the DB.
	* @param Object $entity Entity to be persisted.
	*/
	public function persist($entity);

	/**
	* <b>Updates</b> the info of an entity.
	* @param Object $entity Entity to be updated.
	* @return Object Returns the reloaded entity.
	*/
	public function merge($entity);

	/**
	* <b>Removes</b> an entity from the DB.
	* @param Object $entity Entity to be removed.
	*/
	public function remove($entity);
}
