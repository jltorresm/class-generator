<?php namespace Generator\GClass;

use Monolog\Logger,
	Monolog\Handler\StreamHandler,
	Generator\AbstractFactory;

class ClassFactory implements AbstractFactory
{
	private static $connection; //Reference to AbstractConnection compliant Connection object
	private static $log;

	private static function init()
	{
		self::$log = new Logger(get_called_class());
		self::$log->pushHandler(new StreamHandler(__DIR__ . '/../../log/app.log', Logger::DEBUG));
	}

	public static function create($tableName = null)
	{
		self::init();

		try
		{
			if (!$tableName['name'])
			{
				throw new \Exception("Trying to create class from empty value");
			}

			$className = str_replace(' ', '', ucwords(str_replace(['_', '-'], ' ', $tableName['name'])));

			$class = new PhpClass($className);

			$class->pushMetadata([
				'author' => self::$connection->getUsername(),
				'tableName' => $tableName['name']
			]);

			$attr = [];
			$meth = [];

			foreach (self::$connection->getColumns($tableName['name']) as $column)
			{
				$col = [];
				$col['name'] = strtolower($column['COLUMN_NAME']);
				$col['description'] = $column['COLUMN_COMMENT'];
				$col['nullable'] = $column['IS_NULLABLE'];
				$col['type'] = $column['DATA_TYPE'];
				$col['key'] = $column['COLUMN_KEY'];

				$attr[] = $col;
			}

			$class->setAttributes($attr);
			$class->setMethods(['get', 'set']);

			echo "Read and mapped table '${tableName['name']}' successfully", PHP_EOL;
			self::$log->addInfo("Read and mapped table '${tableName['name']}' successfully");

			return $class;
		}
		catch (\Exception $e)
		{
			echo $e->getMessage(), PHP_EOL;
			self::$log->addError($e->getMessage());
		}
	}

	public static function setConnection ($connection)
	{
		self::$connection = $connection;
	}
}
