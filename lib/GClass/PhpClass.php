<?php namespace Generator\GClass;

use Monolog\Logger,
	Monolog\Handler\StreamHandler;

class PhpClass implements AbstractClass
{
	//json_enconded object (JSON cus of performance 131% over serialize)
	private $_template = '{
		"header":"<?php\\n\\n\/**\\n * Purpose of Class :className.\\n *\\n * @author :author\\n * @tableName :tableName\\n * @version 1.0\\n *\/\\nclass :className {\\n\\n",
		"constructor":"\\tfunction __construct(:params) {\\n:body\\t}\\n",
		"optParam":"$:var = null",
		"param":"$:var",
		"paramSeparator":", ",
		"attr":"\\t\/**\\n\\t * :description\\n\\t * @var :type\\n\\t * @nullable :null\\n\\t * @key :key\\n\\t *\/\\n\\tprivate $:name;\\n\\n",
		"function":"public function :functName(:params) {\\n:body\\n}\\n",
		"get":"\\tpublic function get:name() {\\n\\t\\treturn $this->:name;\\n\\t}\\n",
		"set":"\\tpublic function set:name($:name) {\\n\\t\\t$this->:name = $:name;\\n\\t}\\n",
		"assign":"\\t\\t$this->:var = $:var;\\n",
		"end":"}\\n"
	}';
	private $attributes = [];
	private $methods = [];
	private $meta = [];
	private $name;

	private $log;

	public function __construct ($name = null, $attr = null, $meth = null)
	{
		$this->attributes = $meth;
		$this->methods = $attr;
		$this->name = $name;

		$this->log = new Logger(get_class($this));
		$this->log->pushHandler(new StreamHandler(__DIR__ . '/../../log/app.log', Logger::DEBUG));
	}

	public function render()
	{
		echo "Generating structure for class '$this->name'", PHP_EOL;
		$this->log->addInfo("Generating structure for class '$this->name'");

		$classParts = json_decode($this->_template);

		$structure = $this->renderPart($classParts, 'header', [
			':className' => $this->name,
			':author' => $this->meta['author'],
			':tableName' => $this->meta['tableName']
		]);

		$constructorParams = '';
		$constructorBody = '';
		$accessors = '';

		foreach ($this->attributes as $idx => $attrb) {
			$structure .= $this->renderPart($classParts, 'attr', [
				':description' => $attrb['description'],
				':type' => $attrb['type'],
				':null' => $attrb['nullable'],
				':key' => $attrb['key'],
				':name' => $attrb['name']
			]);

			$constructorParams .= $this->renderParam($classParts, [
				':var' => $attrb['name']
			], true, !isset($this->attributes[$idx + 1]) );

			$constructorBody .= $this->renderPart($classParts, 'assign',[
				':var' => $attrb['name']
			]);

			$accessors .= $this->renderAccesors($classParts, [
				':name' => $attrb['name']
			]);
		}

		$structure .= $this->renderPart($classParts, 'constructor', [
			':params' => $constructorParams,
			':body' => $constructorBody
		]);

		//TODO: To extend functionality a little bit here the generic methods should be rendered.
		//The ones that are set in methods attribute.

		$structure .= "\n$accessors" . $classParts->end;

		return $structure;
	}

	public function __toString ()
	{
		return $this->render();
	}

	public function pushMetadata(array $val)
	{
		$this->meta = array_merge($this->meta, $val);
	}

	public function getAttributes()
	{
		return $this->attributes;
	}

	public function getMethods()
	{
		return $this->methods;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setAttributes(array $attr)
	{
		$this->attributes = $attr;
	}

	public function setMethods(array $meth)
	{
		$this->methods = $meth;
	}

	private function renderPart($template, $part, $tokens)
	{
		return str_replace(array_keys($tokens), array_values($tokens), $template->$part);
	}

	private function renderParam($template, $tokens, $isOptional = true, $isLast = false)
	{
		$param = '';
		if ($isOptional)
		{
			$param .= str_replace(array_keys($tokens), array_values($tokens), $template->optParam);
		}
		else
		{
			$param .= str_replace(array_keys($tokens), array_values($tokens), $template->param);
		}

		if (!$isLast)
		{
			$param .= $template->paramSeparator;
		}

		return $param;
	}

	private function renderAccesors($template, $tokens)
	{
		$accessor = str_replace(array_keys($tokens), array_values($tokens), $template->get);
		$accessor .= str_replace(array_keys($tokens), array_values($tokens), $template->set);

		$accessor = preg_replace_callback('/get(\w+)\(/', function ($val){
			return 'get' . str_replace(' ', '', ucwords(str_replace(['_', '-'], ' ', $val[1]))) . '(';
		}, $accessor);

		$accessor = preg_replace_callback('/set(\w+)\(/', function ($val){
			return 'set' . str_replace(' ', '', ucwords(str_replace(['_', '-'], ' ', $val[1]))) . '(';
		}, $accessor);

		return $accessor;
	}
}
