<?php namespace Generator\GClass;

/**
* Abstract Class
*
* @author joselo
*/
interface AbstractClass
{
	/**
	* Renders the structure of a class with its defined methods and attributes.
	* @return String Rendered structure.
	*/
	public function render();

	/**
	* Returns an array of the defined attributes of the class.
	* @return Array Attributes.
	*/
	public function getAttributes();

	/**
	* Returns an array of the defined methods of the class.
	* @return Array Methods.
	*/
	public function getMethods();

	/**
	* Sets an array as the attributes of the class.
	* @param Array $attr Attributes.
	*/
	public function setAttributes(array $attr);

	/**
	* Sets an array as the methods of the class.
	* @param Array $meth Methods.
	*/
	public function setMethods(array $meth);
}
