<?php

return [
	'db' => [
		'hostname' => '',
		'port' => '',
		'username' => '',
		'password' => '',
		'schema' => '',
		'name' => '',
	],
	'fileDao' => [
		'prefix' => '', //always end on /
	],
];
